<?php

/**
 * @package    JD Builder
 * @author     Team Joomdev <info@joomdev.com>
 * @copyright  2020 www.joomdev.com
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// no direct access
defined('_JEXEC') or die;

$layout = $params->get('layout', 'default');
// Adding Module Class Suffix.

if ($params->get('moduleclass_sfx') != null) {
    $moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'), ENT_COMPAT, 'UTF-8');
} else {
    $moduleclass_sfx = null;
}
require JModuleHelper::getLayoutPath('mod_jdbuilder', $layout);